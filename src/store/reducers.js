import { combineReducers } from 'redux'
import { reducer as formReducer } from 'redux-form'
import checkout from '../modules/Checkout/CheckoutReducer'
import { reducer } from 'react-redux-sweetalert'
import { reducer as burgerMenu } from 'redux-burger-menu'

export const makeRootReducer = (asyncReducers) => {
  return combineReducers({
    checkout,
    sweetalert: reducer,
    form: formReducer,
    burgerMenu
  })
}

export const injectReducer = (store, { key, reducer }) => {
  if (Object.hasOwnProperty.call(store.asyncReducers, key)) return

  store.asyncReducers[key] = reducer
  store.replaceReducer(makeRootReducer(store.asyncReducers))
}

export default makeRootReducer
