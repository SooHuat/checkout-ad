import _ from 'lodash'

export const CalculatePremiumAdPrice = (customer, skus, advertisements) => {
  let price = 0
  Object.keys(skus).forEach((sku) => {
    var advertisement = _.find(advertisements, (ad) => { return ad.id === sku })
    if (advertisement) {
      let promotion = _.find(advertisement.promotion, (promo) => { return promo.customer === customer })
      let unit = skus[sku]
      if (promotion && unit >= promotion.minUnitEntitlePromoPrice) {
        price += _calculateBundlePackagePrice(unit, promotion.bundleInputUnit, promotion.bundleOutputUnit, promotion.promoPrice)
      }
      else {
        price += unit * advertisement.price
      }
    }
  })
  return _.round((price), 2).toFixed(2)
}

function _calculateBundlePackagePrice (numberOfUnit = 0, bundleUnit = 0, bundleOutputUnit = 0, promoPrice = 0) {
  let numberOfPackage = _.floor(numberOfUnit / bundleUnit)
  let packagePrice = numberOfPackage * bundleOutputUnit * promoPrice
  let remainingPrice = numberOfUnit % bundleUnit * promoPrice
  return packagePrice + remainingPrice
}
